from enum import Enum
from datetime import datetime, date, timedelta
from itertools import combinations
from functools import reduce
from math import factorial


class TipoPago(Enum):
    EFE = 'Efectivo'
    TDC = 'Tarjeta'
    OTROS = 'Otros'


class Estado(Enum):
    PENDIENTE = 0
    SALDADO = 1
    NO_ENCONTRADO = 2


class Utils:
    @staticmethod
    def get_movs_fecha(movimientos, fecha, dias):
        min_fecha = fecha - timedelta(dias)
        return [mov for mov in movimientos
                if min_fecha <= mov.fecha <= fecha]

    @staticmethod
    def get_num_combinaciones(n, r):
        return factorial(n) / (factorial(r) * factorial(n - r))


class SaldoBanco:
    def __init__(self, fecha, descripcion, importe, numero, banco):
        self.fecha = fecha
        self.descripcion = descripcion
        self.importe = importe
        self.numero = numero
        self.banco = banco
        self.movimientos = list()
        self.estado = Estado.PENDIENTE
        self.iden_pago()

    @property
    def fecha(self):
        return self._fecha

    @fecha.setter
    def fecha(self, fecha):
        self._fecha = fecha

    @property
    def descripcion(self):
        return self._descripcion

    @descripcion.setter
    def descripcion(self, descripcion):
        self._descripcion = descripcion

    @property
    def importe(self):
        return self._importe

    @importe.setter
    def importe(self, importe):
        self._importe = importe

    @property
    def numero(self):
        return self._numero

    @numero.setter
    def numero(self, numero):
        self._numero = numero

    @property
    def banco(self):
        return self._banco

    @banco.setter
    def banco(self, banco):
        self._banco = banco

    @property
    def tipo_pago(self):
        return self._tipo_pago

    @tipo_pago.setter
    def tipo_pago(self, tipo_pago):
        self._tipo_pago = tipo_pago

    @property
    def suma_importes(self):
        return sum([mov.importe for mov in self.movimientos])

    @property
    def saldado(self):
        if self.importe == self.suma_importes:
            return True
        return False

    def iden_pago(self):
        relacion_leyendas = {
            'efectivo': TipoPago.EFE,
            'efe': TipoPago.EFE,
            'efectiv': TipoPago.EFE,
            'efec': TipoPago.EFE,
            'tarjetas': TipoPago.TDC,
            'ventas credito': TipoPago.TDC,
            'ventas debito': TipoPago.TDC,
            'terminales punto de venta': TipoPago.TDC,
        }
        for rel, key in relacion_leyendas.items():
            if rel in self.descripcion.lower():
                self.tipo_pago = key
                return
        self.tipo_pago = TipoPago.OTROS

    def depura_movs(self, movimientos):
        self.movimientos = [mov for mov in movimientos
                            if mov.importe <= self.importe
                            and not mov.asignado
                            and mov.tipo_pago == self.tipo_pago]

    def asigna_movs(self):
        for mov in self.movimientos:
            mov.asignado = True

    def margen(self, combinacion):
        margen = 100
        margen_bajo = self.importe - margen
        margen_alto = self.importe + margen
        foo = reduce(lambda x, y: y + x, combinacion)
        return margen_bajo <= float(foo) <= margen_alto

    def combinaciones(self, iteracion):
        for combinacion in combinations(self.movimientos, iteracion):
            if (iteracion > 0 and self.margen(combinacion)):
                self.movimientos = list(combinacion)
                self.asigna_movs()
                self.estado = Estado.SALDADO
                print('SALDADO!')
                return True
        print(f'descartamos una combinación {iteracion}')
        return False


    def iteracion_especial(self, repeticiones):
        len_movs = len(self.movimientos)
        print(f'vamos por {len_movs} movimientos')
        abajo = 1
        arriba = len_movs -1
        x = 0
        y = 0
        while x <= len_movs:
            if x % 2 == 0:
                y = arriba
                arriba -= 1
            else:
                y = abajo
                abajo += 1
            if not self.going_deeper(len_movs, y, repeticiones):
               return False
            resultado = self.combinaciones(y)
            if resultado:
                return resultado
            x += 1

    def going_deeper(self, longitud, repeticiones, fijas):
        return Utils.get_num_combinaciones(longitud, repeticiones) <= fijas

    def busca_movs(self, movimientos, repeticiones, rango_dias):
        suma = 0
        suma_resp = 0
        for dias in range(*rango_dias):
            print(f'Día {dias}')
            self.depura_movs(Utils.get_movs_fecha(movimientos, self.fecha,
                                                  dias))
            suma = self.suma_importes
            if suma == suma_resp:
                continue
            suma_resp = suma
            for mov in self.movimientos:
                if mov.importe == self.importe:
                    self.movimientos = [mov]
                    mov.asignado = True
                    self.estado = Estado.SALDADO
                    print('SALDADO!')
                    return True
            if self.saldado:
                self.asigna_movs()
                self.estado = Estado.SALDADO
                print('SALDADO!')
                return True
            if suma > self.importe:
                resultado = self.iteracion_especial(repeticiones)
                if resultado:
                    return True
        self.estado = Estado.NO_ENCONTRADO
        print('Saldo no encontrado')
        return False

    def __repr__(self):
        return (f'{self.fecha}-{self.descripcion}-{self.importe}'
                f'-{self.numero}-{self.banco}')


class MovimientoKepler:
    def __init__(self, fecha, movimiento, sucursal, documento, tipo,
                 descripcion, importe, tipo_poliza, num_poliza, pre_tipo_pago,
                 factura, uuid):
        self.fecha = fecha
        self.movimiento = movimiento
        self.sucursal = sucursal
        self.documento = documento
        self.tipo = tipo
        self.descripcion = descripcion
        self.importe = importe
        self.tipo_poliza = tipo_poliza
        self.num_poliza = num_poliza
        self.pre_tipo_pago = pre_tipo_pago
        self.factura = factura
        self.uuid = uuid
        self.asignado = False
        self.identifica_mov()

    @property
    def fecha(self):
        return self._fecha

    @fecha.setter
    def fecha(self, fecha):
        self._fecha = fecha

    @property
    def movimiento(self):
        return self._movimiento

    @movimiento.setter
    def movimiento(self, movimiento):
        self._movimiento = movimiento

    @property
    def sucursal(self):
        return self._sucursal

    @sucursal.setter
    def sucursal(self, sucursal):
        self._sucursal = sucursal

    @property
    def documento(self):
        return self._documento

    @documento.setter
    def documento(self, documento):
        self._documento = documento

    @property
    def tipo(self):
        return self._tipo

    @tipo.setter
    def tipo(self, tipo):
        self._tipo = tipo

    @property
    def descripcion(self):
        return self._descripcion

    @descripcion.setter
    def descripcion(self, descripcion):
        self._descripcion = descripcion

    @property
    def importe(self):
        return self._importe

    @importe.setter
    def importe(self, importe):
        if importe is None:
            importe = 0
        self._importe = importe

    @property
    def tipo_poliza(self):
        return self._tipo_poliza

    @tipo_poliza.setter
    def tipo_poliza(self, tipo_poliza):
        self._tipo_poliza = tipo_poliza

    @property
    def num_poliza(self):
        return self._num_poliza

    @num_poliza.setter
    def num_poliza(self, num_poliza):
        self._num_poliza = num_poliza

    @property
    def pre_tipo_pago(self):
        return self._pre_tipo_pago

    @pre_tipo_pago.setter
    def pre_tipo_pago(self, pre_tipo_pago):
        self._pre_tipo_pago = pre_tipo_pago

    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        self._uuid = uuid

    @property
    def factura(self):
        return self._factura

    @factura.setter
    def factura(self, factura):
        self._factura = factura

    def ident_mov_ref(self):
        str_mov = str(self.movimiento).lower()
        relacion = {
            'efe': TipoPago.EFE,
            'tdd': TipoPago.TDC,
            'tdc': TipoPago.TDC,
        }
        for llave, tipo in relacion.items():
            if llave in str_mov:
                self.tipo_pago = tipo
                return

    def identifica_mov(self):
        str_tipo_pago = str(self.pre_tipo_pago)
        relacion_movs = {
            'EF': TipoPago.EFE,
            'TD': TipoPago.TDC,
        }
        for llave, tipo in relacion_movs.items():
            if llave in str_tipo_pago:
                self.tipo_pago = tipo
                return
        self.ident_mov_ref()
        self.tipo_pago = TipoPago.OTROS

    def __repr__(self):
        return (f'{self.fecha}|{self.movimiento}|{self.sucursal}|'
                f'{self.documento}|{self.tipo}|{self.descripcion}|'
                f'{self.importe}')

    def __add__(self, other):
        if isinstance(other, MovimientoKepler):
            return self.importe + other.importe
        if isinstance(other, (int, float)):
            return self.importe + other

    def __float__(self):
        return float(self.importe)
